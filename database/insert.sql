/*Inserimento delle tipologie*/

/*Cioccolato*/
INSERT INTO `tipologia` (`nomeTipologia`) VALUES ('Cioccolato');

/*Caramelle*/
INSERT INTO `tipologia` (`nomeTipologia`) VALUES ('Caramelle');

/*Altro*/
INSERT INTO `tipologia` (`nomeTipologia`) VALUES ('Altro');

/*Inserimento dei prodotti*/

/*Barretta al latte*/
INSERT INTO `prodotto` (`codProdotto`, `nomeProdotto`, `descrizione`, `quantità`, `prezzo`, `foto`, `nomeTipologia`)
VALUES (NULL, 'Barretta Wonka al latte', "Ecco a voi la famosissima e rinomata Barretta Wonka con cioccolato al latte. Sarai forse tu il fortunato a vincere un biglietto d'orato?", '100', '2 €', 'bar.jpg', 'Cioccolato');

/*Barretta con murshmellow*/
INSERT INTO `prodotto` (`codProdotto`, `nomeProdotto`, `descrizione`, `quantità`, `prezzo`, `foto`, `nomeTipologia`) VALUES (NULL, 'Whipple-Scrumptious Fudgemallow Delight', "Una favolosa tavoletta di cioccolato al latte e un cuore di murshmellow", '70', '4 €', 'bar2.jpg', 'Cioccolato');

/*donutz*/
INSERT INTO `prodotto` (`codProdotto`, `nomeProdotto`, `descrizione`, `quantità`, `prezzo`, `foto`, `nomeTipologia`) VALUES (NULL, 'Donutz', "Una deliziosa ciambella al cioccolato ripiena di cioccolato: perchè il cioccolato sta benissimo col cioccolato", '100', '2 €', 'donutz.jpg', 'Cioccolato');

/*oompas*/
INSERT INTO `prodotto` (`codProdotto`, `nomeProdotto`, `descrizione`, `quantità`, `prezzo`, `foto`, `nomeTipologia`) VALUES (NULL, 'Peanut Butter Oompas', "Cosa rende il burro d'arachidi delle nostre Peanut Butter Oompas così buono? Le arachidi sono sgusciate da scoiattoli! Solo gli scoiattoli riescono a sbucciarle mantenendole così perfettamente integre", '80', '4 €', 'oompas.jpg', 'Cioccolato');

/*barretta al latte base biscotto*/
INSERT INTO `prodotto` (`codProdotto`, `nomeProdotto`, `descrizione`, `quantità`, `prezzo`, `foto`, `nomeTipologia`) VALUES (NULL, 'Barretta Wonka al latte con base al biscotto', "Un croccante biscotto ricoperto della classica cioccolata Wonka", '100', '4 €', 'red.jpg', 'Cioccolato');

/*super skrunch*/
INSERT INTO `prodotto` (`codProdotto`, `nomeProdotto`, `descrizione`, `quantità`, `prezzo`, `foto`, `nomeTipologia`) VALUES (NULL, 'Super Skrunch', "La classica Peanut Butter Oompas ripiena di riso croccante. Questa barretta metterebbe in difficoltà anche un professionista della masticata del livello di Violetta Beauregarde. ", '100', '3 €', 'superskrunch.jpg', 'Cioccolato');

/*gobstopper*/
INSERT INTO `prodotto` (`codProdotto`, `nomeProdotto`, `descrizione`, `quantità`, `prezzo`, `foto`, `nomeTipologia`) VALUES (NULL, 'Gobstopper', "Confetti per i bambini che non hanno tanti soldi da spendere. Anche se si succhiano un anno intero non rimpiccioliscono. Carini no? 
(Il signor Wonka si esime da ogni e qualsiasi responsabilità, civile e penale, e di risarcimento per eventuali danni a cose, persone, strutture o denti)
", '100', '4 €', 'small.jpg', 'Caramelle');

/*Pixy Stix*/
INSERT INTO `prodotto` (`codProdotto`, `nomeProdotto`, `descrizione`, `quantità`, `prezzo`, `foto`, `nomeTipologia`) VALUES (NULL, 'Pixy Stix', "Mille colori per mille gusti: viola all'uva, azzurro ai frutti tropicali, rosso alla ciliegia e arancione all'arancia. Le caramelle in polvere Wonka Pixy Stix hanno veramente un gusto per tutti!", '70', '4 €', 'pixystix.jpg', 'Caramelle');

/*Bottle Caps*/
INSERT INTO `prodotto` (`codProdotto`, `nomeProdotto`, `descrizione`, `quantità`, `prezzo`, `foto`, `nomeTipologia`) VALUES (NULL, 'Bottle Caps the soda pop candy', "Prima giochi, poi mangi! Caramelle di zucchero a forma di divertenti tappi di bottiglia colorati di diversi gusti, che combinano la dolcezza delle caramelle in compresse con il delizioso frizante della soda!", '100', '4 €', 'wonka-bottle-caps.jpg', 'Caramelle');

/*Bici*/
INSERT INTO `prodotto` (`codProdotto`, `nomeProdotto`, `descrizione`, `quantità`, `prezzo`, `foto`, `nomeTipologia`) VALUES (NULL, 'Bici Wonka', "Niente è più bello di una bella gita fuori porta all'aria fresca!
Con la bicicletta firmata Wonka sfreccerai veloce come un battello di zucchero in un fiume di cioccolata calda.", '30', '25 €', 'bici.jpg', 'Altro');

/*Dentiricio*/
INSERT INTO `prodotto` (`codProdotto`, `nomeProdotto`, `descrizione`, `quantità`, `prezzo`, `foto`, `nomeTipologia`) VALUES (NULL, 'Dentifricio SweeTarts squeez', "Il dentifricio più dolce del mondo! Ai gusti ciliegia e mela.
(Per eventuali carie rivolgersi tempestivamente al Dr. Wilbur Wonka)", '50', '4 €', 'dentifricio.jpg', 'Altro');

/*termos rosa*/
INSERT INTO `prodotto` (`codProdotto`, `nomeProdotto`, `descrizione`, `quantità`, `prezzo`, `foto`, `nomeTipologia`) VALUES (NULL, 'Termos Bar', "Un comodissimo termos per fare in modo che la tua cioccolata calda non si raffreddi nemmeno nelle giornate più fredde d'inverno.", '50', '10 €', 'termosRosa.jpg', 'Altro');

/*termos golden*/
INSERT INTO `prodotto` (`codProdotto`, `nomeProdotto`, `descrizione`, `quantità`, `prezzo`, `foto`, `nomeTipologia`) VALUES (NULL, 'Termos Golden Ticket', "Un comodissimo termos per fare in modo che la tua cioccolata calda non si raffreddi nemmeno nelle giornate più fredde d'inverno.", '50', '10 €', 'termos.jpg', 'Altro');


/*Inserimento degli utenti*/

/*Venditore "Willy Wonka"*/
INSERT INTO `user` (`username`, `nome`, `cognome`, `email`, `password`, `venditore`,`indirizzoPrincipale`) VALUES ('WW', 'Willy', 'Wonka', 'wonkawilly@chocolate.com', '184f6754fb47c2bc3a549b70db9b190363b6fe3a079c7f52d829d2d25952ffd7', '1',"Fabbrica");

/*Cliente "Charlie Bucket"*/
INSERT INTO `user` (`username`, `nome`, `cognome`, `email`, `password`, `venditore`,`indirizzoPrincipale`) VALUES ('CB', 'Charlie', 'Bucket', 'charlieB@chocolate.com', 'ab3604e21eb75668d913bd3fef03b315d34c5d7ca8ed8813161fa91ac89b2c8c', '0', "Casetta");

