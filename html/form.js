const pattern = /^(?:[0-9]+[a-z]|[a-z]+[0-9])[a-z0-9]*$/i;

$(document).ready(function(){
    
    const username = $("input#username");
    const password = $("input#password");

    $("main>form").submit(function(e){
        e.preventDefault();
        let isFormedOk = true;

        const user_value = username.val();
        const pass_value = password.val();

        username.next().remove();
        if(user_value.length < 4){
            username.parent().append("<p> Username deve contenere almeno 4 caratteri. </p>");
            isFormedOk = false;
        }
        password.next().remove();
        if(pass_value==undefined || pass_value.length<8 || !pattern.test(pass_value)){
            password.parent().append("<p>Password deve essere almeno 8 caratteri e deve contenere almeno una lettera ed un numero</p>");
            isFormOk = false;
        }

        if(isFormedOk){
            e.currentTarget.submit();
        }


    });

});