const datiProdottiCioc = [{
    "Nome" : "Barretta di cioccolato fondente",
    "Tipologia" : "Cioccolato",
    "Immagine" : "img/red.jpg",
    "Descrizione" : "Lorem ipsum blablablabla",
    "QuantitàD" : "100",
    "Prezzo" : "1000",
    "Sconto" : "20%"
},
{
    "Nome" : "Skrunch",
    "Tipologia" : "Cioccolato",
    "Immagine" : "img/superskrunckbar.jpg",
    "Descrizione" : "Lorem ipsum blablablabla",
    "QuantitàD" : "90",
    "Prezzo" : "80",
    "Sconto" : "0%"
},
{
    "Nome" : "Donutz",
    "Tipologia" : "Cioccolato",
    "Immagine" : "img/donutz.jpg",
    "Descrizione" : "Lorem ipsum blablablabla",
    "QuantitàD" : "550",
    "Prezzo" : "900",
    "Sconto" : "30%"    
}]

const datiProdottiCar = [{
    "Nome" : "Pixy Stix",
    "Tipologia" : "Caramella",
    "Immagine" : "img/pixystix.jpg",
    "Descrizione" : "Lorem ipsum blablablabla",
    "QuantitàD" : "100",
    "Prezzo" : "1000",
    "Sconto" : "20%"
},
{
    "Nome" : "Gobstopper",
    "Tipologia" : "Caramella",
    "Immagine" : "img/small.jpg",
    "Descrizione" : "Lorem ipsum blablablabla",
    "QuantitàD" : "90",
    "Prezzo" : "80",
    "Sconto" : "0%"
},
{
    "Nome" : "Bottle caps",
    "Tipologia" : "Caramella",
    "Immagine" : "img/wonka-bottle-caps.jpg",
    "Descrizione" : "Lorem ipsum blablablabla",
    "QuantitàD" : "550",
    "Prezzo" : "900",
    "Sconto" : "30%"    
}]

const main = document.querySelector("main"); /*recupero il tag html*/

for(let i = 0; i < datiProdottiCioc.length; i++){
    let articolo = `
    <article>
        <header>
            <div>
                <img src="${datiProdottiCioc[i]["Immagine"]}" alt=""> 
            </div>
            <h2>${datiProdottiCioc[i]["Nome"]}</h2>
        </header>
        <footer>
            <a href="more.html">Please, tell me more!</a>
        </footer>
    </article>
    `;
    main.innerHTML += articolo; 
}




/*
Cioccolato

<article>
            <header>
                <div>
                    <img src="img/red.jpg" alt=""> 
                </div>
                <h2>Barretta di cioccolato fondente</h2>
            </header>
            <footer>
                <a href="more.html">Please, tell me more!</a>
            </footer>
        </article>
        <article>
            <header>
                <div>
                    <img src="img/superskrunckbar.jpg" alt=""> 
                </div>
                <h2>Super Skrunch</h2>
            </header>
            <footer>
                <a href="more.html">Please, tell me more!</a>
            </footer>
        </article>
        <article>
            <header>
                <div>
                    <img src="img/donutz.jpg" alt=""> 
                </div>
                <h2>Donutz</h2>
            </header>
            <footer>
                <a href="more.html">Please, tell me more!</a>
            </footer>
        </article>
*/

/*
Caramelle
<article>
            <header>
                <div>
                    <img src="img/pixystix.jpg" alt=""> 
                </div>
                <h2>Pixy Stix</h2>
            </header>
            <footer>
                <a href="more.html">Please, tell me more!</a>
            </footer>
        </article>
        <article>
            <header>
                <div>
                    <img src="img/small.jpg" alt=""> 
                </div>
                <h2>Gobstopper</h2>
            </header>
            <footer>
                <a href="more.html">Please, tell me more!</a>
            </footer>
        </article>
        <article>
            <header>
                <div>
                    <img src="img/wonka-bottle-caps.jpg" alt=""> 
                </div>
                <h2>Bottle caps</h2>
            </header>
            <footer>
                <a href="more.html">Please, tell me more!</a>
            </footer>
        </article>


*/

/*
Altro

*/