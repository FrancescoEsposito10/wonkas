<?php

require_once("bootstrap.php");

$idProdotto = -1;
if(isset($_GET["id"])){
    $idProdotto = $_GET["id"];
}

if(isUserLoggedIn()){
    $loginVenditore_result = $dbh->checkLoginVenditore($_SESSION["username"]);
    if(count($loginVenditore_result)==1){
        $templateParams["titolo"] = "Area Venditore";
        $templateParams["nome"] = "update-prodotto.php";
        $templateParams["categorie"] = $dbh->getCategories();
        $templateParams["prodotto"] = $dbh->getProductById($idProdotto);
    }

}else{
    header("Location: login.php");
}


require("template/base.php");

?>