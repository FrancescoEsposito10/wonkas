<?php foreach($templateParams["prodottiCioccolato"] as $cioccolato): ?>
<article>
    <header>
        <div>
            <img src="<?php echo UPLOAD_DIR.$cioccolato["foto"]; ?>" alt="" /> 
        </div>
        <h2><?php echo $cioccolato["nomeProdotto"]; ?></h2>
    </header>
    <footer>
        <a href="more.php?id=<?php echo $cioccolato["codProdotto"]; ?>">Please, tell me more!</a>
    </footer>
</article>
<?php endforeach; ?>
