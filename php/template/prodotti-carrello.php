<h2>Carrello</h2>
<?php if(count($templateParams["carrelloUtente"])==0): ?>
    <p> Il carrello è vuoto, <?php echo $_SESSION["nome"];?> corri a fare acquisti! </p>
<?php else: ?>
<?php foreach($templateParams["carrelloUtente"] as $prodcarr): ?>
<article>
    <header>
        <div>
            <img src="<?php echo UPLOAD_DIR.$prodcarr["foto"]; ?>" alt=""> 
        </div>
        <h3><?php echo $prodcarr["nomeProdotto"]; ?></h3>
    </header>
    <div>
        <small>Quantità: </small>
        <strong><?php echo $prodcarr["quantitàInserita"]; ?></strong>
    </div>
    <div>
        <small>Prezzo: </small>
        <strong><?php echo strval($dbh->prezzoTotProdotto($prodcarr["cod"],$_SESSION["username"])[0]["TOTP"]); ?></strong>
    </div>
    <footer>
        <div>
            <a id = "togliDalCarrello" class= "button" href="elimina-prodotto-carrello.php?id=<?php echo $prodcarr["cod"]; ?>"> Togli dal carrello</a>      
        </div>

    </footer>
</article>
<?php endforeach; endif;?>
<article>
    <header>
        <h3>RIEPILOGO ORDINE</h3>
    </header>
    <div>
        <small>Destinatario: </small>
        <strong><?php echo $_SESSION["nome"];?> <?php echo $_SESSION["cognome"];?></strong>
    </div>
    <div>
        <small>Indirizzo: </small>
        <strong><?php echo $dbh->getIndirizzo($_SESSION["username"]);?></strong>
    </div>
    <div>
        <small>Totale: </small>
        <strong><?php echo strval($dbh->prezzoTotCarrello($_SESSION["username"])[0]["TOTC"]);?></strong>
    </div>
    <footer>
    <?php if(count($templateParams["carrelloUtente"])==0): ?>
        <p> Nessun prodotto nel carrello </p>
    <?php else: ?>
        <div>
            <a id = "terminaEPaga" class= "button" href="termina-paga.php?tot=<?php echo strval($dbh->prezzoTotCarrello($_SESSION["username"])[0]["TOTC"]);?>"> Termina e paga</a>
        </div>
    </footer>
    <?php endif; ?>
</article>



