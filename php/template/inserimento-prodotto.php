<form action="insert-new-product.php" method="POST" enctype="multipart/form-data">
    <h2>Inserimento nuovo prodotto</h2>   
    <ul>
        <li>
            <label for="nomeProdotto">Nome:</label><input type="text" id="nomeProdotto" name="nomeProdotto" value="" />
        </li>
        <li>
            <label for="descrizione">Descrizione:</label><textarea id="descrizione" name="descrizione"></textarea>
        </li>
        <li>
            <label for="quantità">Quantità:</label><textarea id="quantità" name="quantità"></textarea>
        </li>
        <li>
            <label for="prezzo">Prezzo:</label><textarea id="prezzo" name="prezzo"></textarea>
        </li>
        <li>
            <label for="foto">Foto prodotto:</label><input type="file" name="foto" id="foto" />
        </li>
        <li>
            <?php foreach($templateParams["categorie"] as $categoria): ?>
            <input type="radio" id="<?php echo $categoria["nomeTipologia"]; ?>" name="categoria" value="<?php echo $categoria["nomeTipologia"]; ?>" checked/>
            <label for="<?php echo $categoria["nomeTipologia"]; ?>"><?php echo $categoria["nomeTipologia"]; ?></label>
            <br/>
            <?php endforeach; ?>
        </li>
        <li>
            <input type="submit" class = "button" name="submit" value="Salva" />
            <a href="login.php">Annulla</a>
        </li>
    </ul>
    <input type = "hidden" name=" action" value="1" />
</form>