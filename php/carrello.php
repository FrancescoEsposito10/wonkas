<?php

require_once("bootstrap.php");

$templateParams["titolo"] = "Carrello";
$templateParams["nome"] = "prodotti-carrello.php";
$templateParams["js"] = array("js/jquery-3.4.1.min.js", "js/notificaTermina.js");

$templateParams["carrelloUtente"] = $dbh->getProdottiCarrelloUtente($_SESSION["username"]);


require("template/base.php");
?>