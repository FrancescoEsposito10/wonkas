<?php

require_once("bootstrap.php");
$prodottiCioc = $dbh->getRandomChoco(1);
$prodottiCan =$dbh->getRandomCandy(1);
$prodottiOth =$dbh->getRandomOther(1);

for($i = 0; $i < count($prodottiCioc); $i++){
    $prodottiCioc[$i]["foto"] = UPLOAD_DIR.$prodottiCioc[$i]["foto"];
}
header("Content-Type: application/json");
echo json_encode($prodottiCioc);

for($i = 0; $i < count($prodottiCan); $i++){
    $prodottiCan[$i]["foto"] = UPLOAD_DIR.$prodottiCan[$i]["foto"];
}
header("Content-Type: application/json");
echo json_encode($prodottiCan);

for($i = 0; $i < count($prodottiOth); $i++){
    $prodottiOth[$i]["foto"] = UPLOAD_DIR.$prodottiOth[$i]["foto"];
}
header("Content-Type: application/json");
echo json_encode($prodottiOth);

?>