function generaProdCioc(datiProdottiCioc){
    let result = "";

    for(let i=0; i < datiProdottiCioc.length; i++){
        let prodCioc = `
        <article>
            <header>
                <div>
                    <img src="${datiProdottiCioc[i]["foto"]}" alt=""> 
                </div>
                <h2>${datiProdottiCioc[i]["nomeProdotto"]}</h2>
            </header>
            <footer>
                <a href="more.php">Please, tell me more!</a>
            </footer>
        </article>
        `;
        result += prodCioc;
    }
    return result;
}

function generaProdCan(datiProdottiCan){
    let result = "";

    for(let i=0; i < datiProdottiCan.length; i++){
        let prodCan = `
        <article>
            <header>
                <div>
                    <img src="${datiProdottiCan[i]["foto"]}" alt=""> 
                </div>
                <h2>${datiProdottiCan[i]["nomeProdotto"]}</h2>
            </header>
            <footer>
                <a href="more.php">Please, tell me more!</a>
            </footer>
        </article>
        `;
        result += prodCan;
    }
    return result;
}

function generaProdOth(datiProdottiOth){
    let result = "";

    for(let i=0; i < datiProdottiOth.length; i++){
        let prodOth = `
        <article>
            <header>
                <div>
                    <img src="${datiProdottiOth[i]["foto"]}" alt=""> 
                </div>
                <h2>${datiProdottiOth[i]["nomeProdotto"]}</h2>
            </header>
            <footer>
                <a href="more.php">Please, tell me more!</a>
            </footer>
        </article>
        `;
        result += prodOth;
    }
    return result;
}

$(document).ready(function(){
    $.getJSON("api-prodotto.php", function(data){
        let proCioc = generaProdCioc(data);
        let proCan = generaProdCan(data);
        let proOth = generaProdOth(data);
        const main = $("main");
        main.append(proCioc);
        main.append(proCan);
        main.append(proOth);
    });
});

