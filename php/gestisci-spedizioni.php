<?php

require_once("bootstrap.php");

if(isUserLoggedIn()){
    $loginVenditore_result = $dbh->checkLoginVenditore($_SESSION["username"]);
    if(count($loginVenditore_result)==1){
        $templateParams["titolo"] = "Area Venditore";
        $templateParams["nome"] = "gestione-stati-prodotti.php";
        $templateParams["prodottiAcquistati"] = $dbh->getProdottiAcquistati();
    }

}else{
    header("Location: login.php");
}


require("template/base.php");

?>