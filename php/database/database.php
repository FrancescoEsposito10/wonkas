<?php

class DatabaseHelper{
    private $db;

    public function __construct($servername, $username, $password, $dbname, $port){
        $this->db = new mysqli($servername, $username, $password, $dbname, $port);
        if($this->db->connect_error){
            die("Connessione al DB fallita"); 
        }

    }

    /**metodo per estrarre prodotti al cioccolato random */
    public function getRandomChoco($n=1){
        $stmt = $this->db->prepare("SELECT codProdotto, nomeProdotto, foto, nomeTipologia FROM prodotto WHERE nomeTipologia = 'Cioccolato' ORDER BY RAND() LIMIT ?");
        $stmt->bind_param("i",$n);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    /**metodo per estrarre caramelle random */
    public function getRandomCandy($n=1){
        $stmt = $this->db->prepare("SELECT codProdotto, nomeProdotto, foto, nomeTipologia FROM prodotto WHERE nomeTipologia = 'Caramelle' ORDER BY RAND() LIMIT ?");
        $stmt->bind_param("i",$n);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    /**metodo per estrarre altri prodotti random */
    public function getRandomOther($n=1){
        $stmt = $this->db->prepare("SELECT codProdotto, nomeProdotto, foto, nomeTipologia FROM prodotto WHERE nomeTipologia = 'Altro' ORDER BY RAND() LIMIT ?");
        $stmt->bind_param("i",$n);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    /**metodo che controlla se username e password passati esistono nel db*/
    public function checkLogin($username, $password){
        $stmt = $this->db->prepare("SELECT username, nome, venditore, cognome, email, punti, indirizzoPrincipale FROM user WHERE username = ? AND password = ?");
        $stmt->bind_param("ss", $username, $password);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    /**metodo che controlla se l'username passato corrisponde al profilo del venditore */
    public function checkLoginVenditore($username){
        $stmt = $this->db->prepare("SELECT username, nome, venditore FROM user WHERE username = ? AND venditore = 1");
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    /*recuper un prodotto dato il suo id*/
    public function getProductById($id){
        $stmt = $this->db->prepare("SELECT codProdotto, nomeProdotto, descrizione, quantità, prezzo, foto, nomeTipologia FROM prodotto WHERE codProdotto = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);

    }

    /**recupera il nome di un prodotto e la sua foto dato l'id */
    public function getNomeFotoProdotto($id){
        $stmt = $this->db->prepare("SELECT nomeProdotto, foto FROM prodotto WHERE codProdotto = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);

    }

    /**ritorna i prodotti presenti in un carrello ed i loro attributi dato l'id del proprietario */
    public function getProdottiCarrelloUtente($username){
            $stmt = $this->db->prepare("SELECT nomeProdotto, quantitàInserita, prezzo, data_ora, P.codProdotto AS cod, P.foto AS foto
                                        FROM prodotto_carrello PC, prodotto P
                                        WHERE userCarrello = ?
                                        AND PC.prodottoCarrello = P.codProdotto");
            $stmt->bind_param("s", $username);
            $stmt->execute();
            $result = $stmt->get_result();
        
            return $result->fetch_all(MYSQLI_ASSOC);
    }

    /**ritorna tutte le categorie previste dal db */
    public function getCategories(){
        $stmt = $this->db->prepare("SELECT nomeTipologia FROM tipologia");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    /**ritorna la categoria dato l'id */
    public function getCategoryById($idcategory){
        $stmt = $this->db->prepare("SELECT nomeTipologia FROM tipologia WHERE nomeTipologia=?");
        $stmt->bind_param('i',$idcategory);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    /**ritorna l'insieme dei prodotti appartenenti alla categoria passata */
    public function getPostByCategory($idcategory){
        $query = "SELECT codProdotto, nomeProdotto, descrizione, quantità, prezzo, foto, nomeTipologia FROM prodotto, tipologia WHERE tipologia=? AND nomeTipologia=nomeTipologia";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$idcategory);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    /**ritorna tutti i prodotti presenti nel db */
    public function getAllProducts(){
        $stmt = $this->db->prepare("SELECT * FROM prodotto ");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    /**ritorna tutti i prodotti della categoria cioccolato */
    public function getChocolateP(){
        $stmt = $this->db->prepare("SELECT codProdotto, nomeProdotto, foto, nomeTipologia FROM prodotto WHERE nomeTipologia = 'Cioccolato'");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    /**ritorna tutti i prodotti della categoria caramelle */
    public function getCandyP(){
        $stmt = $this->db->prepare("SELECT codProdotto, nomeProdotto, foto, nomeTipologia FROM prodotto WHERE nomeTipologia = 'Caramelle'");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    /**ritorna tutti i prodotti della categoria altro */
    public function getOtherP(){
        $stmt = $this->db->prepare("SELECT codProdotto, nomeProdotto, foto, nomeTipologia FROM prodotto WHERE nomeTipologia = 'Altro'");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    /**metodo per l'aggiunta di un prodotto al carrello */
    public function inserisciCarrello($data, $quantità, $user, $prodotto)
    {
        $stmt = $this->db->prepare("SELECT COUNT(*) as num
                                    FROM prodotto_carrello
                                    WHERE userCarrello = ?
                                    AND prodottoCarrello = ?");
        $stmt->bind_param("ss", $user, $prodotto);
        $stmt->execute();
        $result = $stmt->get_result();
        $result = $result->fetch_all(MYSQLI_ASSOC);

        if ($result[0]["num"] == 0) {
        $stmt = $this->db->prepare("INSERT INTO prodotto_carrello
        (data_ora, quantitàInserita, userCarrello, prodottoCarrello)
        VALUES (?, ?, ?, ?)");
        $stmt->bind_param("ssss", $data, $quantità, $user, $prodotto);
        $stmt->execute();
        } else {
        $stmt = $this->db->prepare("SELECT quantitàInserita
                                    FROM prodotto_carrello
                                    WHERE userCarrello = ?
                                    AND prodottoCarrello = ?");
        $stmt->bind_param("ss", $user, $prodotto);
        $stmt->execute();
        $result = $stmt->get_result();
        $result = $result->fetch_all(MYSQLI_ASSOC);

        $stmt = $this->db->prepare("DELETE FROM prodotto_carrello
                                    WHERE userCarrello = ?
                                    AND prodottoCarrello = ?");
        $stmt->bind_param("ss",  $user, $prodotto);
        $stmt->execute();

        $tot = 0;
        foreach ($result as $qtà) {
            $tot += $qtà["quantitàInserita"];
        }
        $newQtà = $quantità + $tot;
        $stmt = $this->db->prepare("INSERT INTO prodotto_carrello
        (data_ora, quantitàInserita, userCarrello, prodottoCarrello)
        VALUES (? , ?, ?, ?)");
        $stmt->bind_param("ssss", $data, $newQtà, $user, $prodotto);
        $stmt->execute();
        }
    }

    /**calcolo prezzo totale su 1 prodotto */
    public function prezzoTotProdotto($codProd, $username){
        $stmt = $this->db->prepare("SELECT P.prezzo*PC.quantitàInserita AS TOTP
                                    FROM prodotto P, prodotto_carrello PC
                                    WHERE P.codProdotto = ? AND P.codProdotto = PC.prodottoCarrello AND PC.userCarrello = ?");
        $stmt->bind_param("ss", $codProd, $username);
        $stmt->execute();
        $result = $stmt->get_result();
    
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    /**calcolo prezzo totale su tutto carrello */
    public function prezzoTotCarrello($username){
        $stmt = $this->db->prepare("SELECT SUM(P.prezzo*PC.quantitàInserita) AS TOTC
                                    FROM prodotto P, prodotto_carrello PC
                                    WHERE P.codProdotto = PC.prodottoCarrello AND PC.userCarrello = ?");
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();
    
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    /**elimina il prodotto passato dal carrello passato */
    public function eliminaProdottoCarrello($user, $codProd){
        $stmt = $this->db->prepare("DELETE FROM prodotto_carrello
                                    WHERE userCarrello = ?
                                    AND prodottoCarrello = ?");
        $stmt->bind_param("ss",  $user, $codProd);
        $stmt->execute();

    }

    /**ritorna l'indirizzo dato l'utente */
    public function getIndirizzo($user){
        $stmt = $this->db->prepare("SELECT indirizzoPrincipale
                                    FROM user
                                    WHERE username = ?");
        $stmt->bind_param("s", $user);
        $stmt->execute();
        $result = $stmt->get_result();
    
        return $result->fetch_all(MYSQLI_ASSOC)[0]["indirizzoPrincipale"];
    }

    /**Trasferisce i prodotti dal carrello agli acquisti effettuati dato l'utente*/
    public function terminaPaga($user){
        $stmt = $this->db->prepare("SELECT COUNT(*) as num
                                    FROM prodotto_carrello
                                    WHERE userCarrello = ?");
        $stmt->bind_param("s", $user);
        $stmt->execute();
        $result = $stmt->get_result();
        $result = $result->fetch_all(MYSQLI_ASSOC);

        if ($result[0]["num"] != 0) {
            $prodotti = $this->getProdottiCarrelloUtente($user);
            $data = date("Y-m-d H:i:sa");
            $stato = "Ordine confermato";
            $indirizzo = $this->getIndirizzo($user);
            foreach($prodotti as $pd){
                $prodotto = $pd["cod"];
                $quantità = $pd["quantitàInserita"];
                $stmt = $this->db->prepare("INSERT INTO prodotto_acquistato
                                            VALUES (?,?,?,?,?,?)");
                $stmt->bind_param("ssssss", $quantità, $stato, $data, $user, $prodotto, $indirizzo);
                $stmt->execute();

                $stmt = $this->db->prepare("DELETE FROM prodotto_carrello
                                            WHERE userCarrello = ?");
                $stmt->bind_param("s", $user);
                $stmt->execute();
            }
        }
    }

    /**aggiorna la quantità di un prodotto in magazzino dato l'id dell'utente che ha effettuato l'ultimo acquisto */
    public function updateQuantità($user){
        $stmt = $this->db->prepare("SELECT quantitàInserita, prodottoCarrello
                                    FROM prodotto_carrello
                                    WHERE userCarrello = ?");
        $stmt->bind_param("s", $user);
        $stmt->execute();
        $result = $stmt->get_result();
        $query1 = $result->fetch_all(MYSQLI_ASSOC);
        foreach($query1 as $res){
            $stmt = $this->db->prepare("SELECT quantità as Tot
                                FROM prodotto
                                WHERE codProdotto = ?");
            $stmt->bind_param("s", $res["prodottoCarrello"]);
            $stmt->execute();
            $result = $stmt->get_result();
            $result = $result->fetch_all(MYSQLI_ASSOC);

            $nuovaQuantità = ( intval($result["Tot"])-intval($res["quantitàInserita"])); 
            $stmt= $this->db->prepare("UPDATE prodotto
                                        SET quantità = ?
                                        WHERE codProdotto = ? ");
            $stmt->bind_param("ss", $nuovaQuantità, $res["prodottoCarrello"]);
            $stmt->execute();
        }
    }

    /**aggiunge un nuovo utente nel db associandogli gli attributi passati */
    public function registrazioneUtente($username, $nome, $cognome, $email, $password, $indirizzoPrincipale){
        $venditore = "0";
        $punti = "0";
        $stmt = $this->db->prepare("INSERT INTO user
                                    VALUES (?,?,?,?,?,?,?,?)");
        $stmt->bind_param("ssssssss", $username, $nome, $cognome, $email, $password, $venditore,$punti, $indirizzoPrincipale);
        $stmt->execute();
    }

    /**ritorna lo storico degli ordini di un cliente dato l'username */
    public function riepilogoOrdini($user){
        $stmt = $this->db->prepare("SELECT *
                                    FROM prodotto_acquistato
                                    WHERE username = ? ");
        $stmt->bind_param("s", $user);
        $stmt->execute();
        $result = $stmt->get_result();
    
        return $result->fetch_all(MYSQLI_ASSOC);

    }

    /**inserisce un nuovo prodotto nel db associandogli gli attributi passati */
    public function insertNewProduct($nomeProdotto, $descrizione, $quantità,$prezzo, $foto, $nomeTipologia){
        $stmt = $this->db->prepare("SELECT COUNT(codProdotto) as num
                                    FROM prodotto");
        $stmt->bind_param("s", $user);
        $stmt->execute();
        $result = $stmt->get_result();
        $idProdotto = $result->fetch_all(MYSQLI_ASSOC)[0]["num"];
        $idProdotto = $idProdotto + 1;

        $stmt = $this->db->prepare("INSERT INTO prodotto (codProdotto, nomeProdotto, descrizione, quantità, prezzo, foto, nomeTipologia)
                                    VALUES (?,?,?,?,?,?,?)");
        $stmt->bind_param("sssssss",$idProdotto,$nomeProdotto, $descrizione, $quantità,$prezzo, $foto, $nomeTipologia);
        $stmt->execute();
    }

    /**aggiorna gli attributi di un prodotto dato il prodoto ed i nuovi attributi */
    public function updateProduct($codProdotto, $quantità){
        $stmt = $this->db->prepare("UPDATE prodotto 
                                    SET quantità = ?
                                    WHERE codProdotto = ?");
        $stmt->bind_param('ss', $quantità,$codProdotto);
        
        return $stmt->execute();
    }

    /**cancella un prodotto dal db dato il prodotto */
    public function deleteProduct($codProdotto){
        $stmt = $this->db->prepare("DELETE FROM prodotto
                                    WHERE codProdotto = ?");
        $stmt->bind_param("s", $codProdotto);
        $stmt->execute();

    }

    /**ritorna l'indirizzo email dato l'username dell'utente */
    public function getEmail($user){
        $stmt = $this->db->prepare("SELECT email
                                    FROM user
                                    WHERE username = ? ");
        $stmt->bind_param("s", $user);
        $stmt->execute();
        $result = $stmt->get_result();
    
        return $result->fetch_all(MYSQLI_ASSOC);

    }

    /**ritorna i punti accumulati da un utente dato il suo username */
    public function getPunti($user){
        $stmt = $this->db->prepare("SELECT punti
                                    FROM user
                                    WHERE username = ? ");
        $stmt->bind_param("s", $user);
        $stmt->execute();
        $result = $stmt->get_result();
    
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    /**imposta i punti di un utente dato il suo id e l'ammontare della spesa */
    public function setPunti($spesa, $user){
        $punti = $this->getPunti($user);
        $punti = strval(intval($punti[0]["punti"]) + (intval($spesa)*3));
        $stmt = $this->db->prepare("UPDATE user
                                    SET punti = ?
                                    WHERE username = ?");
        $stmt->bind_param("ss", $punti,$user);
        return  $stmt->execute();

    }

    /**azzera il punteggio dell'utente dato il suo username */
    public function azzeroPunteggio($user){
        $punti = "0";
        $stmt = $this->db->prepare("UPDATE user
                                    SET punti = ?
                                    WHERE username = ?");
        $stmt->bind_param("ss", $punti,$user);
        return  $stmt->execute();

    }

    /**aggiunge una tavoletta di cioccolato ai prodotti acquistati dall'utente passato */
    public function aggiungiRegalo($user){
        $quantitàComprata = "1";
        $statoConsegna = "Ordine confermato";
        $data_ora = date("Y-m-d H:i:sa");
        $codProdotto = "1";
        $indirizzo = $this->getIndirizzo($user);

        $stmt = $this->db->prepare("INSERT INTO prodotto_acquistato 
                                    VALUES (?,?,?,?,?,?)");
        $stmt->bind_param("ssssss",$quantitàComprata, $statoConsegna, $data_ora, $user, $codProdotto, $indirizzo);
        $stmt->execute();

    }
    
        /**restituisce tutti i prodotti acquistati che aspettano di essere spediti */
        public function getProdottiAcquistati(){
            $statoConsegna = "Ordine confermato";
            $stmt = $this->db->prepare("SELECT *
                                        FROM prodotto_acquistato
                                        WHERE statoConsegna = ?");
            $stmt->bind_param("s", $statoConsegna);
            $stmt->execute();
            $result = $stmt->get_result();
        
            return $result->fetch_all(MYSQLI_ASSOC);
    
        }

        /**cambia lo stato di consegna di un prodotto, dati l'id del pordotto e l'utente */
        public function spedisciProdottoAcquistato($user, $codProdotto){
            $stato1 = "Ordine confermato";
            $stato2 = "Ordine spedito";
            $stmt = $this->db->prepare("UPDATE prodotto_acquistato
                                        SET statoConsegna = ?
                                        WHERE username = ? AND codProdotto = ? AND statoConsegna = ?");
            $stmt->bind_param("ssss", $stato2,$user, $codProdotto, $stato1);
            return  $stmt->execute();
        }







}

?>