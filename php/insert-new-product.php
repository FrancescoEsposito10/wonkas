<?php
require_once("bootstrap.php");

if(!isUserLoggedIn() || !isset($_POST["action"])){
    header("Location: login.php");
}

if($_POST["action"]==1){
    $nomeProdotto = $_POST["nomeProdotto"];
    $descrizione = $_POST["descrizione"];
    $quantità = $_POST["quantità"];
    $prezzo = $_POST["prezzo"];
    $categoria_inserita = "Cioccolato";
    $categoria_inserita = $_POST["categoria"];
    list($result, $msg) = uploadImage(UPLOAD_DIR, $_FILES["foto"]);
    if($result == 1){
        $foto = $msg;
        $codProdotto = $dbh->insertNewProduct($nomeProdotto, $descrizione, $quantità,$prezzo, $foto, $categoria_inserita);
    }

    header("Location: login.php");
}

if($_POST["action"]==2){
    $codProdotto = $_POST["codProdotto"];
    $nomeProdotto = $_POST["nomeProdotto"];
    $quantità = $_POST["quantità"];
    $dbh->updateProduct($codProdotto,$quantità);
    header("Location: login.php");
}

require("template/base.php");

?>

