<?php

require_once("bootstrap.php");

$templateParams["titolo"] = "Prodotti";
$templateParams["nome"] = "prodotti-tipologia.php";
$templateParams["categorie"] = $dbh->getCategories();

$idcategoria = -1;
if(isset($_GET["id"])){
    $idcategoria = $_GET["id"];
}
$nomecategoria = $dbh->getCategoryById($idcategoria);
$templateParams["titolo"] = "".$nomecategoria[0]["nomecategoria"]; 
$templateParams["articoli"] = $dbh->getPostByCategory($idcategoria)[0];

require("template/base.php");
?>

<!-- codice per prodotti tipologia se decide di andare il foreach-->

<?php foreach($templateParams["articoli"]  as $categoria): ?>
<article>
    <header>
        <div>
            <img src="<?php echo UPLOAD_DIR.$categoria["foto"]; ?>" alt="" /> 
        </div>
        <h2><?php echo $categoria["nomeProdotto"]; ?></h2>
    </header>
    <footer>
        <a href="more.php?id=<?php echo $categoria["codProdotto"]; ?>">Please, tell me more!</a>
    </footer>
</article>
<?php endforeach; ?>