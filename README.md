# CREAZIONE DATABASE #

Per creare il database:

-copiare il contenuto di database/creazione_db.sql e incollarlo in XAMPP

Per popolare il database:

-selezionare il database "wonka" copiare il contenuto di database/insert.sql e incollarlo in XAMPP.


# ACCESSO #
Sono stati creati due utenti di prova:

-VENDITORE     username: WW     password: ChocoBar1

-CLIENTE 	   username: CB	    password: goldenTicket1


Per creare un nuovo cliente, invece, nella pagina di login basta selezionare il link "Registrati".


# DESIGN BRIEF #

-PROBLEMA

Il mondo ha bisogno di dolcezza, ecco perchè Wonka vuole dare la possibilità alle persone di tutto il mondo di comprare e gustarsi i suoi fantastici prodotti.

-A CHI E' RIVOLTO

Wonka's si rivolge ad un pubblico vasto e senza età: non servono i denti per mangiare i dolci!

-VINCOLI

Rivolgendosi a persone di ogni età il sito deve risultare di facile fruizione anche per le persone meno pratiche.

-PRODOTTO FINALE

Wonka's si presenta con uno stile semplice, intuito ed accessibile, mobile firts.

-TIMELINE

Il sito deve essere ultimato prima dell'inizio della scuola, così che genitori e nonni possano premiare figli e nipoti per i buoni risultati. 

-RESEARCH

Dalle nostre ricerche esistono altri negozi simili ma la nostra cioccolata è mischiata con una cascata, che lo gira bene, lo rende leggero e spumoso. Nessun'altra fabbrica mischia il cioccolato con una cascata e su questo non ci piove!
